def myfunction(arg1, arg2):
    """
    Ничего не делает, просто демонстрирует синтаксис документации.
    
    Это пример как читаемая для человека документация может быть
    интерпретирована doxypypy. Принимает на вход два числа.
    Суммирует аргументы и делит первый на второй.

    Args:
        arg1:   Первый аргумент.
        arg2:   Второй аргумент.

    Returns:
        Результат вычислений.

    Raises:
        ZeroDivisionError, AssertionError, & ValueError.

    Examples:
        >>> myfunction(2, 3)
        '5 - 0'
        >>> myfunction(5, 0)
        Traceback (most recent call last):
            ...
        ZeroDivisionError: integer division or modulo by zero
        >>> myfunction(23.5, 23)
        Traceback (most recent call last):
            ...
        AssertionError
        >>> myfunction(5, 50)
        Traceback (most recent call last):
            ...
        ValueError
    """
    assert isinstance(arg1, int)
    if arg2 > 23:
        raise ValueError
    return '{0} - {1}'.format(arg1 + arg2, arg1 / arg2)